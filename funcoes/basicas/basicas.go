package main

import (
	"fmt"
)

//Função que não tem parâmetro nem retorno.
func f1() {
	fmt.Println("Entrou em F1...")
}

//Função com parâmetros e sem retorno.
func f2(p1 string, p2 string) {
	fmt.Printf("F2: %s %s\n", p1, p2)
}

//Dunçaõ sem parâmetros e retorno simples.
func f3() string {
	return "String de F3"
}

//Função com parâmetros declarados de forma diferente e retorno string
func f4(p1, p2 string) string {
	//Essa função não printa na tela, formata uma
	return fmt.Sprintf("String de F4 com parâmetros: %s e %s", p1, p2)
}

//Funçaão com retorno múltiplo...
func f5() (string, string) {
	return "Retorno 1", "Retorno 2"
}

func main() {

	f1()

	f2("P1", "P2")

	fmt.Println("Printando o retrno de F3:", f3())

	fmt.Println("Printando o retrno de F4:", f4("P1", "P2"))

	retF5_1, retF5_2 := f5()
	fmt.Println("Retornos múltiplos da função f5:", retF5_1, retF5_2)

	//Chamando duas funções ao mesmo tempo e armazenando seus retornos...
	retF3, retF4 := f3(), f4("Parâmetro 1", "Parâmetro 2")
	fmt.Printf("Retorno de f3: %s e Retorno de f4: %s", retF3, retF4)
}
