package main

import (
	"fmt"
)

func multiplicacao(a, b float64) float64 {
	return a * b
}

func soma(a, b float64) float64 {
	return a + b
}

func exec(f1 func(float64, float64) float64, p1, p2 float64) float64 {
	return f1(p1, p2)
}

func main() {
	fmt.Printf("Resultado da execução da função: %.2f\n", exec(multiplicacao, 3, 6))
	fmt.Printf("Resultado da execução da função: %.2f\n", exec(soma, 3, 6))
}
