package main

import (
	"fmt"
)

func obterValorAprovad(numero int) int {
	fmt.Println("Começo de obterValorAprovado com parâmetro", numero)
	defer fmt.Println("Fim de obterValorAprovado!")
	if numero >= 5000 {
		fmt.Println("Valor alto demais...")
		return 5000
	}
	fmt.Println("Valor baixo...")
	return numero
}
func main() {
	fmt.Println("Começo de programa principal!")
	defer fmt.Println("Fim de programa principal!")

	valor := obterValorAprovad(5600)
	fmt.Println("Retorno:", valor)

	valor = obterValorAprovad(3000)
	fmt.Println("Retorno:", valor)
}
