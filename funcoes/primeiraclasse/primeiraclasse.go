package main

import (
	"fmt"
)

var soma = func(a, b int) int {
	return a + b
}

func main() {
	fmt.Println("Somando...", soma(1, 5))

	sub := func(a, b int) int {
		return a - b
	}

	fmt.Println("Subtraindo...", sub(1, 5))
}
