package main

import (
	"fmt"
	"runtime/debug"
)

func f3() {
	fmt.Println("Entrou no f3...")
	debug.PrintStack()
	fmt.Println("Saiu do f3...")
}

func f2() {
	fmt.Println("Entrou no f2...")
	f3()
	fmt.Println("Saiu do f2...")
}

func f1() {
	fmt.Println("Entrou no f1...")
	f2()
	fmt.Println("Saiu do f1...")
}
func main() {
	fmt.Println("Começou...")
	f1()
	fmt.Println("Terminou...")
}
