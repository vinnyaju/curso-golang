package main

import (
	"fmt"
)

func inc1(n int) {
	n++
}

//Revisão: ponteiro é representado por um *
func inc2(n *int) {
	//O * é usado para desreferenciar um poteiro, ter acesso ao valor ao qual o ponteiro aponta
	*n++
}
func main() {
	n := 1
	inc1(n) //incremento por valor...
	fmt.Println("Valor de n:", n)

	//o & obtém o endereço da variável...
	inc2(&n) //incremento por referência...
	fmt.Println("Valor de n:", n)

}
