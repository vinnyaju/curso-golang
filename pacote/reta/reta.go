package main

import (
	"math"
)

//Inicializando com letra maiúscula é PÚBLICO (visível dentro e fora do pacote)!
//Inicializando com letra minúscula é PRIVADO (visível apenas dentro do pacote)!

//Exemplo..
//Ponto - gera um ponto público
//ponto / _Ponto - gera um ponto privado

//Ponto é um ponto público e representa uma coordenada no plano cartesiado.
type Ponto struct {
	x, y float64
}

func catetos(a, b Ponto) (cx, cy float64) {
	cx = math.Abs(b.x - a.x)
	cy = math.Abs(b.y - a.y)
	return
}

//Distancia calcula a distância entre os pontos a e b fornecidos como parâmetros
func Distancia(a, b Ponto) float64 {
	cx, cy := catetos(a, b)
	return math.Sqrt(math.Pow(cx, 2) + math.Pow(cy, 2))
}
