package main

import (
	"fmt"
)

func notaParaConceito(n float64) string {
	if n >= 9 && n <= 10 {
		return "A"
	} else if n >= 8 && n < 9 {
		return "B"
	} else if n >= 5 && n < 8 {
		return "C"
	} else if n >= 3 && n < 5 {
		return "D"
	} else {
		return "E"
	}
}

//desafio de refatoração
func notaParaConceitoRefatorado(n float64) string {
	switch {
	case n >= 9 && n <= 10:
		return "A"
	case n >= 8 && n < 9:
		return "B"
	case n >= 5 && n < 8:
		return "C"
	case n >= 3 && n < 5:
		return "D"
	default:
		return "E"
	}
}

func main() {
	fmt.Printf("\nO conceito do aluno é %v e %v", notaParaConceito(9), notaParaConceitoRefatorado(9))
	fmt.Printf("\nO conceito do aluno é %v e %v", notaParaConceito(8.1), notaParaConceitoRefatorado(8.1))
	fmt.Printf("\nO conceito do aluno é %v e %v", notaParaConceito(5), notaParaConceitoRefatorado(5))
	fmt.Printf("\nO conceito do aluno é %v e %v", notaParaConceito(3.1), notaParaConceitoRefatorado(3.1))
	fmt.Printf("\nO conceito do aluno é %v e %v", notaParaConceito(0), notaParaConceitoRefatorado(0))
}
