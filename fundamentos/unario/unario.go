package main

import (
	"fmt"
)

func main() {

	x, y := 1, 2

	//Apenas POSTFIX
	x++ //x += 1 ou x = x + 1
	y--

	fmt.Println("X:", x)
	fmt.Println("Y:", y)
}
