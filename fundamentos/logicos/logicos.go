package main

import (
	"fmt"
)

func compras(trab1, trab2 bool) (bool, bool, bool) {
	comprarTV50 := trab1 && trab2
	comprarTV32 := trab1 != trab2 //equivalente ao ou exclusivo
	comprarSorteve := trab1 || trab2
	return comprarTV50, comprarTV32, comprarSorteve
}

func main() {

	tv50, tv32, tomarSorvete := compras(false, false)

	fmt.Printf("Tv50: %t, Tv32: %t, Sorvete: %t, Saudável: %t",
		tv50, tv32, tomarSorvete, !tomarSorvete)
}
