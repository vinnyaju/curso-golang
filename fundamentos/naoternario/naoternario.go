package main

import (
	"fmt"
)

// ObterResultado mostra uma alternativa não ter operador ternário, use o if
func ObterResultado(nota float64) string {
	if nota >= 6.0 {
		return "Aprovado"
	}
	return "Reprovado"
}
func main() {

	fmt.Println("Resultado:", ObterResultado(6.1))
}
