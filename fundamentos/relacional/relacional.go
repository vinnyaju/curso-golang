package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Strings: ", "Banana" == "Banana")

	a, b := 3, 2

	fmt.Printf("Usando para a e b iguais a %v e %v respectivamente...\n", a, b)
	fmt.Println("a == b:", a == b)
	fmt.Println("a != b:", a != b)
	fmt.Println("a <  b:", a < b)
	fmt.Println("a <= b:", a <= b)
	fmt.Println("a >  b:", a > b)
	fmt.Println("a >= b:", a >= b)

	d1 := time.Unix(0, 0)
	d2 := time.Unix(0, 0)

	fmt.Printf("D1 igual a D2?: %v\n", d1 == d2)
	fmt.Printf("D1 equals  D2?: %v\n", d1.Equal(d2))

	type Pessoa struct {
		Nome string
	}

	p1 := Pessoa{"João"}
	p2 := Pessoa{"João"}

	fmt.Printf("P1 == P2?: %v\n", p1 == p2)

}
