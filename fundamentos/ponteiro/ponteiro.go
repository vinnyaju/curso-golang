package main

import "fmt"

func main() {

	i := 1
	fmt.Println("i:", i)

	var p *int
	p = &i
	fmt.Println("p:", p)

	*p = 2
	fmt.Println("i:", i)

	*p++
	fmt.Println("i:", i)

	//Não pode fazer aritimética com ponteiro, a linha abaixo é inválida.
	//p++

	fmt.Println(p, *p, i, &i)
}
