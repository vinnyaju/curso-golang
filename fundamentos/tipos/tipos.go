package main

import (
	"fmt"
	"math"
	"reflect"
)

func main() {
	// Números inteiros
	fmt.Println(1, 2, 1000)
	fmt.Println("Literal inteiro é:", reflect.TypeOf(32000))

	// Sem sinal... uint8 uint16 uint32 uint64

	var b byte = 255
	fmt.Println("O byte é:", reflect.TypeOf(b))

	// Com sinal... int8 int16 int32 int64

	i1 := math.MaxInt64
	fmt.Println("O valor máximo do int é", i1)
	fmt.Println("O tipo do i1 é", reflect.TypeOf(i1))

	var i2 = 'a'
	fmt.Println("o rune é", reflect.TypeOf(i2))
	fmt.Println(i2)

	// números reais float32 e float64
	var x float32 = 49.99
	fmt.Println("O tipo de x é", reflect.TypeOf(x))
	fmt.Println("Otipo da literal 49.99 é", reflect.TypeOf(49.99))

	// booleans
	bo := true
	fmt.Println("O tipo de bo é", reflect.TypeOf(bo))
	fmt.Println(!bo)

	// string
	s1 := "Olá meu nome é Marcus"
	fmt.Println(s1 + "!")
	fmt.Println("O tamanho da da string é", len(s1))

	// string com múltiplas linhas
	s2 := `opa
	quase que foi`
	fmt.Println("O tamanho da da string é", len(s2))

	// char??
	char := 'a'
	fmt.Println("o char na verdade é", reflect.TypeOf(char))
}
