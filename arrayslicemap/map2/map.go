package main

import (
	"fmt"
)

func main() {
	funcionarios := map[string]float64{
		"José João":       11315.25,
		"Gabriela Silva":  10315.25,
		"Pedro Júnior":    9315.25,
		"Marcus Vinicius": 8315.25,
	}

	funcionarios["Teodora"] = 5685.25

	delete(funcionarios, "ChaveInexistente")
	fmt.Println(funcionarios)

	for nome, salario := range funcionarios {
		fmt.Printf("Nome: %s, Salário: %.2f\n", nome, salario)
	}
}
