package main

import (
	"fmt"
)

func main() {
	//Criou um slice apontando para um array interno com 10 posições
	s := make([]int, 10)
	s[9] = 12 //atribuiu 12 à última posição

	fmt.Println(s)

	//Atribuiu um slice novo à mesma variável s com tamanho 10 e que tem um array interno com capacidade de 20 posições
	s = make([]int, 10, 20)

	fmt.Println(s, len(s), cap(s)) //imprime o slice, o tamanho dele e a capacidade do array interno

	s = append(s, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0) //Adiciona 10 números ao slice que já tinha 10 posições, ainda temos capacidade de 20, ou seja, encheu.

	fmt.Println(s, len(s), cap(s)) //imprime o slice, o tamanho dele e a capacidade do array interno

	s = append(s, 1) //Adiciona mais um número ao slice já cheio com array interno cheio

	//Aumentou o slice pra 21 posições e dobrou a capacidade do array interno para 40
	fmt.Println(s, len(s), cap(s))

	//Ou seja, o slice manipulado aponta automaticamente para novos arrays para comportar o que lhe é adicionado.
}
