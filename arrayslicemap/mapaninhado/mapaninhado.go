package main

import (
	"fmt"
)

func imprimirMapaCompleto(mapa map[string]map[string]float64) {
	fmt.Println()
	fmt.Print("Imprimindo mapa... ", mapa)
	for letra, listaFunc := range mapa {
		fmt.Println()
		fmt.Println("Letra:", letra)
		for nome, salario := range listaFunc {
			fmt.Printf("Nome: %s, Salário: %.2f\n", nome, salario)
		}
	}
}

func main() {
	funcionariosPorLetra := map[string]map[string]float64{
		"G": {
			"Gabriela Silva": 15456.78,
			"Guga Pereira":   123456.5,
		},
		"J": {
			"José João": 8596.8,
		},
		"P": {
			"Pedro": 4500.65,
		},
	}

	imprimirMapaCompleto(funcionariosPorLetra)

	delete(funcionariosPorLetra, "P")
	imprimirMapaCompleto(funcionariosPorLetra)
}
