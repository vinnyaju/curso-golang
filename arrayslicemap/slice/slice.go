package main

import (
	"fmt"
	"reflect"
)

func main() {
	a1 := [3]int{1, 2, 3} //array
	s1 := []int{1, 2, 3}  //slice

	fmt.Println(a1, s1)
	fmt.Println(reflect.TypeOf(a1), reflect.TypeOf(s1))

	a2 := [5]int{1, 2, 3, 4, 5}
	//Slice não é um array, é um pedaço de um array.
	s2 := a2[1:3]
	s3 := a2[:2] //mais um slice, apontando para o mesmo array a2
	fmt.Println(a2, s2, s3)

	//Slice tem um tamanho e um ponteiro a partir do qual ele ponta continuamente pelo tamanho
	s4 := s2[:1] //Slice de um Slice
	fmt.Println(s2, s4)
}
