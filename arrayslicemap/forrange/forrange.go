package main

import (
	"fmt"
)

func main() {
	numeros := [...]int{1, 2, 3, 4, 5}

	fmt.Println(numeros)

	//pegando o índice e o número em si
	for i, numero := range numeros {
		fmt.Printf("%d) %d\n", i+1, numero)
	}

	//ignorando o índice
	for _, numero := range numeros {
		fmt.Println(numero)
	}
}
