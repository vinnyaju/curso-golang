package main

import (
	"fmt"
)

func main() {
	//Mapas devem ser inicializados
	//var aprovados map[int]string

	aprovados := make(map[int]string)

	aprovados[810463598] = "Maria"
	aprovados[810463599] = "Paulo"
	aprovados[810463600] = "Ana"

	fmt.Println(aprovados)

	//Listando todos...
	for cpf, nome := range aprovados {
		fmt.Printf("%s (CPF: %d)\n", nome, cpf)
	}

	//Recuperando apenas um item...
	fmt.Println("Nome:", aprovados[810463598])

	//Apagando um elemento...
	delete(aprovados, 810463598)
	fmt.Println("Nome que foi apagado:", aprovados[810463598])

	//listando novamente de pois de apagar
	for cpf, nome := range aprovados {
		fmt.Printf("%s (CPF: %d)\n", nome, cpf)
	}

}
