package main

import (
	"fmt"
	"reflect"
)

//Curso é apenas para teste...
type Curso struct {
	nome      string
	sobrenome string
}

func main() {
	var coisa interface{}
	fmt.Println(coisa)
	fmt.Println(reflect.TypeOf(coisa))

	coisa = 3
	fmt.Println(coisa)
	fmt.Println(reflect.TypeOf(coisa))

	coisa = "WOWOWOW"
	fmt.Println(coisa)
	fmt.Println(reflect.TypeOf(coisa))

	coisa = Curso{"Curso", "de teste..."}
	fmt.Println(coisa)
	fmt.Println(reflect.TypeOf(coisa))
}
