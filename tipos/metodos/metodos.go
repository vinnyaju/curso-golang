package main

import (
	"fmt"
	"strings"
)

//Pessoa representa uma pessoa
type Pessoa struct {
	nome      string
	sobrenome string
}

//GetNomeCompleto retorna o nome completo em apenas uma string
func (p Pessoa) GetNomeCompleto() string {
	return p.nome + " " + p.sobrenome
}

//SetNomeCompleto quebra uma string e atribui aos campos da struct separadamente...
func (p *Pessoa) SetNomeCompleto(nomeCompleto string) {
	partes := strings.Split(nomeCompleto, " ")
	p.nome = partes[0]
	p.sobrenome = partes[1]
}
func main() {
	var marcus Pessoa
	marcus = Pessoa{
		nome:      "Marcus",
		sobrenome: "Rodrigues",
	}

	pedro := Pessoa{"Pedro", "Silva"}

	fmt.Println(marcus.GetNomeCompleto())
	fmt.Println(pedro.GetNomeCompleto())

	pedro.SetNomeCompleto("Maria Pereira")
	fmt.Println(pedro.GetNomeCompleto())

}
