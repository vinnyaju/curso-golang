package main

import (
	"fmt"
)

//Esportivo é um tipo de carro...
type Esportivo interface {
	ligarTurbo()
}

//Luxuoso é um tipo de carro...
type Luxuoso interface {
	fazerbaliza()
}

//EsportivoLuxuoso é um tipo de carro...
type EsportivoLuxuoso interface {
	Esportivo
	Luxuoso
	//Pode adicionar mais métodos ainda...
}

//Bmw7 é um carro específico...
type Bmw7 struct{}

func (b Bmw7) ligarTurbo() {
	fmt.Println("Ligando turbo...")
}
func (b Bmw7) fazerbaliza() {
	fmt.Println("Fazendo Baliza...")
}

func main() {
	var b EsportivoLuxuoso = Bmw7{}

	b.ligarTurbo()
	b.fazerbaliza()
}
