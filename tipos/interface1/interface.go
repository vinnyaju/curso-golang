package main

import (
	"fmt"
)

type imprimivel interface {
	toString() string
}

//Pessoa é uma pessoa
type Pessoa struct {
	nome      string
	sobrenome string
}

//Produto é um produto
type Produto struct {
	nome  string
	preco float64
}

//interfaces são implicitamente implementadas
func (p Pessoa) toString() string {
	return p.nome + " " + p.sobrenome
}

func (p Produto) toString() string {
	return fmt.Sprintf("%s : R$ %.2f", p.nome, p.preco)
}

func imprimir(x imprimivel) {
	fmt.Println(x.toString())
}
func main() {
	var coisa imprimivel

	coisa = Pessoa{"Marcus", "Andrade"}
	imprimir(coisa)

	coisa = Produto{"Bola", 15.60}
	imprimir(coisa)
}
