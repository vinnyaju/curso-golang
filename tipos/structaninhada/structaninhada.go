package main

import (
	"fmt"
)

//Item é um item
type Item struct {
	produtoID  int
	quantidate int
	preco      float64
}

//Pedido é um pedido...
type Pedido struct {
	userID int
	itens  []Item
}

func (p Pedido) calculaValorTotal() float64 {
	total := 0.0
	for _, item := range p.itens {
		total += item.preco * float64(item.quantidate)
	}
	return total
}
func main() {
	pedido := Pedido{
		userID: 1,
		itens: []Item{
			Item{1, 2, 12.10},
			Item{3, 2, 23.50},
			Item{21, 100, 1.95},
		},
	}
	fmt.Printf("O valor total é %.2f\n", pedido.calculaValorTotal())
}
