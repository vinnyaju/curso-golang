package main

import (
	"fmt"
)

//Esportivo é uma interface
type Esportivo interface {
	ligarTurbo()
}

//Ferrari é um Esportivo phoda demais...
type Ferrari struct {
	modelo      string
	turboLigado bool
	velocidade  int
}

func (f *Ferrari) ligarTurbo() {
	f.turboLigado = true
}
func main() {
	carro1 := Ferrari{"F40", false, 0}
	carro1.ligarTurbo()

	carro2 := &Ferrari{"F50", false, 0}
	carro2.ligarTurbo()

	//var carro3 Esportivo = Ferrari{"Porsche", false, 0} - Aqui dá pau, tem que usar o endereço quando usar a interface.
	var carro3 Esportivo = &Ferrari{"Porsche", false, 0}

	fmt.Println(carro1, carro2, carro3)
}
